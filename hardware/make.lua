add_rules("mode.debug", "mode.release")

-- 请设置MCU架构，如cortex-m3、cortex-m4
local mcu = "cortex-m4"

-- 请设置宏定义
function set_add_defines()
    -- 说明：宏定义即为Keil中C/C++的Define配置，请将add_defines中的配置按照格式配置为自己的，最后一条结尾请问包含","号
    add_defines(
        "USE_STDPERIPH_DRIVER",
        "STM32F427_437xx",
        "USE_STM324x9I_EVAL",
        "USE_USB_OTG_FS",
        "USE_ULPI_PHY"
    )
end

-- 请设置需要编译的.c源文件相对路径
function set_add_files()
    -- 说明：请将add_files中的配置按照格式配置为自己的，最后一条结尾请问包含","号
    -- 方式一：编译指定路径下的所有.c源文件，则使用："./System/core/src/*.c"
    -- 方式二：编译指定路径下的某一个.c源文件，则使用："./System/core/src/file.c"
    add_files(
        "./System/startup_gcc.s",
        "./System/core/src/*.c",
        "./System/driver/src/*.c",
        "./System/usb/USB_Driver/src/usb_bsp.c",
        "./System/usb/USB_Driver/src/usb_core.c",
        "./System/usb/USB_Driver/src/usb_dcd.c",
        "./System/usb/USB_Driver/src/usb_dcd_int.c",
        "./System/usb/USB_Library/Core/src/usbd_cdc_vcp.c",
        "./System/usb/USB_Library/Core/src/usbd_core.c",
        "./System/usb/USB_Library/Core/src/usbd_desc.c",
        "./System/usb/USB_Library/Core/src/usbd_ioreq.c",
        "./System/usb/USB_Library/Core/src/usbd_req.c",
        "./System/usb/USB_Library/Core/src/usbd_usr.c",
        "./System/usb/USB_Library/Class/cdc/src/usbd_cdc_core.c"
    )
end

-- 请设置要排除的.c源文件
function set_remove_files()
    -- 说明：请将remove_files中的配置按照格式配置为自己的，最后一条结尾请问包含","号
    -- 方式一：排除指定路径下的某一个.c源文件，则使用："./System/core/src/file.c"
    remove_files(
        "./System/driver/src/stm32f4xx_fsmc.c"
    )
end

-- 请设置设置编译所需的.h头文件目录
function set_add_includedirs()
    -- 说明：请将add_includedirs中的配置按照格式配置为自己的，最后一条结尾请问包含","号
    -- 方式一：编译指定路径下的所有.h头文件，则使用："./System/core/inc"
    add_includedirs(
        "./System/core/inc",
        "./System/driver/inc",
        "./System/usb/USB_Driver/inc",
        "./System/usb/USB_Library/Class/cdc/inc",
        "./System/usb/USB_Library/Core/inc"
    )
end

-- 从此处开始，以下的配置信息非必要则无需任何改动，如必须改动，请与GEEKROS技术人员确认
set_plat("cross")

set_arch(muc)

function use_toolchain(sdk_path)
	toolchain("arm-gcc")
		set_kind("cross")
		set_sdkdir(sdk_path)
		set_toolset("cc", "arm-none-eabi-gcc")
		set_toolset("ld", "arm-none-eabi-gcc")
		set_toolset("ar", "arm-none-eabi-ar")
		set_toolset("as", "arm-none-eabi-gcc")
	toolchain_end()
	set_toolchains("arm-gcc")
end

local arm_gcc_installing_path = os.getenv("ARM_GCC_TOOL")
use_toolchain(arm_gcc_installing_path)

target("firmware")

    set_kind("binary")

    set_add_defines()

    set_add_files()

    add_files(
        "./FreeRTOS/*.c",
        "./FreeRTOS/portable/GCC/ARM_CM4F/port.c",
        "./FreeRTOS/portable/MemMang/heap_4.c",
        "./Source/app/src/*.c",
        "./Source/main/src/*.c",
        "./Source/modular/src/*.c",
        "./Source/robot/base/src/*.c",
        "./Source/robot/mini_dog/src/*.c",
        "./Source/utils/src/*.c"
    )

    set_remove_files()

    set_add_includedirs()

    add_includedirs(
        "./Source/app/inc",
        "./Source/main/inc",
        "./Source/modular/inc",
        "./Source/robot/base/inc",
        "./Source/robot/mini_dog/inc",
        "./Source/utils/inc",
        "./FreeRTOS/include",
        "./FreeRTOS/portable/GCC/ARM_CM4F"
    )

    add_cxflags(
        "-mcpu="..mcu,
        "-mthumb",
        "-mfloat-abi=hard -mfpu=fpv4-sp-d16",
        "-Wall -fdata-sections -ffunction-sections",
        "-O3 -g -gdwarf-2",{force = true}
    )
    add_asflags(
        "-mcpu="..mcu,
        "-mthumb",
        "-mfloat-abi=hard -mfpu=fpv4-sp-d16",
        "-Wall -fdata-sections -ffunction-sections",
        "-O3 -g -gdwarf-2",{force = true}
    )
    add_ldflags(
        "-O3",
        "-mcpu="..mcu,
        "-mfloat-abi=hard -mfpu=fpv4-sp-d16",
        "-specs=nano.specs",
        "-L./",
        "-TSystem/flash.ld",
        "-Wl,--gc-sections -std=gnu11 -D_MICROLIB",
        "-lc -lm -lnosys -u _printf_float",{force = true}
    )

    set_targetdir("Build")

    set_filename("firmware.elf")

    -- 生成HEX、BIN文件
    after_build(function(target)
        os.exec("arm-none-eabi-objcopy -O ihex ./Build/firmware.elf ./Build/firmware.hex")
        os.exec("arm-none-eabi-objcopy -O binary ./Build/firmware.elf ./Build/firmware.bin")
        print("---------- Complete Build ---------")
    end)
