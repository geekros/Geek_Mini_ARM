/**
 ******************************************************************************
 * @file    modular.h
 * @author  GEEKROS,  site:www.geekros.com
 ******************************************************************************
 */

#ifndef MODULAR
#define MODULAR

#include "led.h"
#include "usb.h"
#include "buzzer.h"
#include "can.h"
#include "power.h"
#include "key.h"
#include "adc.h"
#include "io.h"
#include "pwm.h"
#include "cpu.h"
#include "flash.h"
#include "mpu.h"
#include "rocker.h"
#include "wifi.h"
#include "servo.h"
#include "hmi.h"
#include "radar.h"
#include "pwm_motor.h"

void Usb_State_Task(void);

#endif
