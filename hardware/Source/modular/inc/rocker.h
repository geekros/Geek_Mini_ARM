/**
 ******************************************************************************
 * @file    rocker.h
 * @author  GEEKROS,  site:www.geekros.com
 ******************************************************************************
 */

#ifndef MODULAR_REMOTE_CONTROL
#define MODULAR_REMOTE_CONTROL

#include <stm32f4xx.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "usb.h"

#define ROCKER_BUFFER 128

typedef struct
{
	int16_t ch0;
	int16_t ch1;
	int16_t ch2;
	int16_t ch3;
	int16_t ch4;
	char s1;
	char s2;
	volatile unsigned char buffer[ROCKER_BUFFER];
} Rocker_Struct;

extern Rocker_Struct Rocker_Data;

Rocker_Struct *Return_Rocker_Address(void);

void Rocker_Init(int baud_rate);

void Rocker_Write_Data(int s1, int s2, float ch0, float ch1, float ch2, float ch3);

void Rocker_Usb_Callback(char *type, int rate, int s1, int s2, float ch0, float ch1, float ch2, float ch3);

#endif
