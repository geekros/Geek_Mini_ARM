/**
 ******************************************************************************
 * @file    can.h
 * @author  GEEKROS,  site:www.geekros.com
 ******************************************************************************
 */

#ifndef MODULAR_CAN
#define MODULAR_CAN

#include <stm32f4xx.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "usb.h"

#define CAN_CHANNEL_LEN 2
#define CAN_MOTOR_LEN 6

typedef struct
{
	CanRxMsg Pid[CAN_CHANNEL_LEN][CAN_MOTOR_LEN];
	CanTxMsg Can_Write_Data[CAN_CHANNEL_LEN][CAN_MOTOR_LEN];
} Can_Struct;

typedef struct
{
    uint16_t Single_Position; // 0-360/100 1度读数100 
    int16_t Position; // 圈/100 1圈读数100 
    int16_t Speed;// turn/min 圈/分钟
    int16_t Current;// A/100 1A读数100
}Can_Current_Struct;

extern Can_Struct Can_Data;

extern Can_Current_Struct Can_Current_Data[CAN_MOTOR_LEN];

void Can_Init(void);

void Can1_Init(void);

void Can1_Data_Callback(CanRxMsg can_read_data);

void Can_Send(int StdId, int16_t Data1, int16_t Data2, int16_t Data3, int16_t Data4);

void Can_Task(void);

void Can_Usb_Callback(char *type, int channel, int motor_id);

#endif
