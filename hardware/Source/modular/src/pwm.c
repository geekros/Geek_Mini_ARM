/**
 ******************************************************************************
 * @file    pwm.c
 * @author  GEEKROS,  site:www.geekros.com
 ******************************************************************************
 */

#include "pwm.h"

/*******************************************************************************
 * @funtion      : Pwm_Init
 * @description  : 模块初始化
 * @param         {*}
 * @return        {*}
 *******************************************************************************/
void Pwm_Init(void)
{
    TIM1_Config(PWM_RESOLUTION, TIM_PSC_APB2, SERVO_DEFAULT_DUTY, PWM_CounterMode_Down, PWMO_OCMode_PWM1, PWM_OCPolarity_High);
    TIM8_Config(PWM_RESOLUTION, TIM_PSC_APB2, SERVO_DEFAULT_DUTY, PWM_CounterMode_Down, PWMO_OCMode_PWM1, PWM_OCPolarity_High);
    TIM2_Config(PWM_RESOLUTION, TIM_PSC_APB1, SERVO_DEFAULT_DUTY, PWM_CounterMode_Down, PWMO_OCMode_PWM1, PWM_OCPolarity_High);
    TIM4_Config(PWM_RESOLUTION, TIM_PSC_APB1, SERVO_DEFAULT_DUTY, PWM_CounterMode_Down, PWMO_OCMode_PWM1, PWM_OCPolarity_High);
    TIM5_Config(PWM_RESOLUTION, TIM_PSC_APB1, SERVO_DEFAULT_DUTY, PWM_CounterMode_Down, PWMO_OCMode_PWM1, PWM_OCPolarity_High);
    for (int i = 1; i < 17; i++)
    {
        Pwm_Motion(i, 50, 1500);
    }
}

/*******************************************************************************
 * @funtion      : TIM1_Config、TIM8_Config、TIM2_Config、TIM4_Config、TIM5_Config
 * @description  : PWM模块所需的时钟配置函数
 * @param         {*}
 * @return        {*}
 *******************************************************************************/
void TIM1_Config(int PWM_Period, int PWM_Prescaler, int PWM_Pluse, PWM_CounterMode_Struct PWM_CounterMode, PWM_OCMode_Struct PWM_OCMode, PWM_OCPolarity_Struct PWM_OCPolarity)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;

    TIM_OCInitTypeDef TIM_OCInitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOE, ENABLE);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13 | GPIO_Pin_14;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOE, &GPIO_InitStructure);

    GPIO_PinAFConfig(GPIOA, GPIO_PinSource8, GPIO_AF_TIM1);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_TIM1);
    GPIO_PinAFConfig(GPIOE, GPIO_PinSource13, GPIO_AF_TIM1);
    GPIO_PinAFConfig(GPIOE, GPIO_PinSource14, GPIO_AF_TIM1);

    TIM_TimeBaseStructure.TIM_Period = PWM_Period;
    TIM_TimeBaseStructure.TIM_Prescaler = PWM_Prescaler;
    TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;

    if (PWM_CounterMode == PWM_CounterMode_Up)
    {
        TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    }

    if (PWM_CounterMode == PWM_CounterMode_Down)
    {
        TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Down;
    }

    TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure);

    if (PWM_OCMode == PWMO_OCMode_PWM1)
    {
        TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
    }

    if (PWM_OCMode == PWM_OCMode_PWM2)
    {
        TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;
    }

    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = PWM_Pluse;

    if (PWM_OCPolarity == PWM_OCPolarity_High)
    {
        TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
    }

    if (PWM_OCPolarity == PWM_OCPolarity_Low)
    {
        TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
    }

    TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Disable;
    TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_High;

    TIM_OC1Init(TIM1, &TIM_OCInitStructure);

    TIM_OC1PreloadConfig(TIM1, TIM_OCPreload_Enable);

    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = PWM_Pluse;

    TIM_OC2Init(TIM1, &TIM_OCInitStructure);

    TIM_OC2PreloadConfig(TIM1, TIM_OCPreload_Enable);

    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = PWM_Pluse;

    TIM_OC3Init(TIM1, &TIM_OCInitStructure);

    TIM_OC3PreloadConfig(TIM1, TIM_OCPreload_Enable);

    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = PWM_Pluse;

    TIM_OC4Init(TIM1, &TIM_OCInitStructure);
    TIM_OC4PreloadConfig(TIM1, TIM_OCPreload_Enable);

    TIM_ARRPreloadConfig(TIM1, ENABLE);
    TIM_CtrlPWMOutputs(TIM1, ENABLE);

    TIM_Cmd(TIM1, ENABLE);
}

void TIM8_Config(int PWM_Period, int PWM_Prescaler, int PWM_Pluse, PWM_CounterMode_Struct PWM_CounterMode, PWM_OCMode_Struct PWM_OCMode, PWM_OCPolarity_Struct PWM_OCPolarity)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;

    TIM_OCInitTypeDef TIM_OCInitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM8, ENABLE);

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOI, ENABLE);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOI, &GPIO_InitStructure);

    GPIO_PinAFConfig(GPIOI, GPIO_PinSource2, GPIO_AF_TIM8);
    GPIO_PinAFConfig(GPIOI, GPIO_PinSource5, GPIO_AF_TIM8);
    GPIO_PinAFConfig(GPIOI, GPIO_PinSource6, GPIO_AF_TIM8);
    GPIO_PinAFConfig(GPIOI, GPIO_PinSource7, GPIO_AF_TIM8);

    TIM_TimeBaseStructure.TIM_Period = PWM_Period;
    TIM_TimeBaseStructure.TIM_Prescaler = PWM_Prescaler;
    TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;

    if (PWM_CounterMode == PWM_CounterMode_Up)
    {
        TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    }
    if (PWM_CounterMode == PWM_CounterMode_Down)
    {
        TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Down;
    }

    TIM_TimeBaseInit(TIM8, &TIM_TimeBaseStructure);

    if (PWM_OCMode == PWMO_OCMode_PWM1)
    {
        TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
    }
    if (PWM_OCMode == PWM_OCMode_PWM2)
    {
        TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;
    }

    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = PWM_Pluse;

    if (PWM_OCPolarity == PWM_OCPolarity_High)
    {
        TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
    }

    if (PWM_OCPolarity == PWM_OCPolarity_Low)
    {
        TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
    }

    TIM_OCInitStructure.TIM_OCNIdleState = TIM_OutputNState_Disable;
    TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_High;

    TIM_OC1Init(TIM8, &TIM_OCInitStructure);
    TIM_OC1PreloadConfig(TIM8, TIM_OCPreload_Enable);

    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = PWM_Pluse;

    TIM_OC2Init(TIM8, &TIM_OCInitStructure);
    TIM_OC2PreloadConfig(TIM8, TIM_OCPreload_Enable);

    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = PWM_Pluse;

    TIM_OC3Init(TIM8, &TIM_OCInitStructure);
    TIM_OC3PreloadConfig(TIM8, TIM_OCPreload_Enable);

    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = PWM_Pluse;

    TIM_OC4Init(TIM8, &TIM_OCInitStructure);
    TIM_OC4PreloadConfig(TIM8, TIM_OCPreload_Enable);

    TIM_ARRPreloadConfig(TIM8, ENABLE);
    TIM_CtrlPWMOutputs(TIM8, ENABLE);

    TIM_Cmd(TIM8, ENABLE);
}

void TIM2_Config(int PWM_Period, int PWM_Prescaler, int PWM_Pluse, PWM_CounterMode_Struct PWM_CounterMode, PWM_OCMode_Struct PWM_OCMode, PWM_OCPolarity_Struct PWM_OCPolarity)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;

    TIM_OCInitTypeDef TIM_OCInitStructure;

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    GPIO_PinAFConfig(GPIOA, GPIO_PinSource0, GPIO_AF_TIM2);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource1, GPIO_AF_TIM2);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_TIM2);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_TIM2);

    TIM_TimeBaseStructure.TIM_Period = PWM_Period;
    TIM_TimeBaseStructure.TIM_Prescaler = PWM_Prescaler;
    TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;

    if (PWM_CounterMode == PWM_CounterMode_Up)
    {
        TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    }

    if (PWM_CounterMode == PWM_CounterMode_Down)
    {
        TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Down;
    }

    TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);

    if (PWM_OCMode == PWMO_OCMode_PWM1)
    {
        TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
    }

    if (PWM_OCMode == PWM_OCMode_PWM2)
    {
        TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;
    }

    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = PWM_Pluse;

    if (PWM_OCPolarity == PWM_OCPolarity_High)
    {
        TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
    }

    if (PWM_OCPolarity == PWM_OCPolarity_Low)
    {
        TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
    }

    TIM_OC1Init(TIM2, &TIM_OCInitStructure);
    TIM_OC1PreloadConfig(TIM2, TIM_OCPreload_Enable);

    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = PWM_Pluse;

    TIM_OC2Init(TIM2, &TIM_OCInitStructure);
    TIM_OC2PreloadConfig(TIM2, TIM_OCPreload_Enable);

    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = PWM_Pluse;

    TIM_OC3Init(TIM2, &TIM_OCInitStructure);
    TIM_OC3PreloadConfig(TIM2, TIM_OCPreload_Enable);

    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = PWM_Pluse;

    TIM_OC4Init(TIM2, &TIM_OCInitStructure);
    TIM_OC4PreloadConfig(TIM2, TIM_OCPreload_Enable);

    TIM_ARRPreloadConfig(TIM2, ENABLE);

    TIM_Cmd(TIM2, ENABLE);
}

void TIM4_Config(int PWM_Period, int PWM_Prescaler, int PWM_Pluse, PWM_CounterMode_Struct PWM_CounterMode, PWM_OCMode_Struct PWM_OCMode, PWM_OCPolarity_Struct PWM_OCPolarity)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;

    TIM_OCInitTypeDef TIM_OCInitStructure;

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOD, &GPIO_InitStructure);

    GPIO_PinAFConfig(GPIOD, GPIO_PinSource12, GPIO_AF_TIM4);
    GPIO_PinAFConfig(GPIOD, GPIO_PinSource13, GPIO_AF_TIM4);
    GPIO_PinAFConfig(GPIOD, GPIO_PinSource14, GPIO_AF_TIM4);
    GPIO_PinAFConfig(GPIOD, GPIO_PinSource15, GPIO_AF_TIM4);

    TIM_TimeBaseStructure.TIM_Period = PWM_Period;
    TIM_TimeBaseStructure.TIM_Prescaler = PWM_Prescaler;
    TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;

    if (PWM_CounterMode == PWM_CounterMode_Up)
    {
        TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    }

    if (PWM_CounterMode == PWM_CounterMode_Down)
    {
        TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Down;
    }

    TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure);

    if (PWM_OCMode == PWMO_OCMode_PWM1)
    {
        TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
    }

    if (PWM_OCMode == PWM_OCMode_PWM2)
    {
        TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;
    }

    if (PWM_OCPolarity == PWM_OCPolarity_High)
    {
        TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
    }

    if (PWM_OCPolarity == PWM_OCPolarity_Low)
    {
        TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
    }

    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = PWM_Pluse;

    TIM_OC1Init(TIM4, &TIM_OCInitStructure);
    TIM_OC1PreloadConfig(TIM4, TIM_OCPreload_Enable);

    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = PWM_Pluse;

    TIM_OC2Init(TIM4, &TIM_OCInitStructure);
    TIM_OC2PreloadConfig(TIM4, TIM_OCPreload_Enable);

    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = PWM_Pluse;

    TIM_OC3Init(TIM4, &TIM_OCInitStructure);
    TIM_OC3PreloadConfig(TIM4, TIM_OCPreload_Enable);

    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = PWM_Pluse;

    TIM_OC4Init(TIM4, &TIM_OCInitStructure);
    TIM_OC4PreloadConfig(TIM4, TIM_OCPreload_Enable);

    TIM_ARRPreloadConfig(TIM4, ENABLE);

    TIM_Cmd(TIM4, ENABLE);
}

void TIM5_Config(int PWM_Period, int PWM_Prescaler, int PWM_Pluse, PWM_CounterMode_Struct PWM_CounterMode, PWM_OCMode_Struct PWM_OCMode, PWM_OCPolarity_Struct PWM_OCPolarity)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;

    TIM_OCInitTypeDef TIM_OCInitStructure;

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM5, ENABLE);

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOH | RCC_AHB1Periph_GPIOI, ENABLE);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOH, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOI, &GPIO_InitStructure);

    GPIO_PinAFConfig(GPIOH, GPIO_PinSource10, GPIO_AF_TIM5);
    GPIO_PinAFConfig(GPIOH, GPIO_PinSource11, GPIO_AF_TIM5);
    GPIO_PinAFConfig(GPIOH, GPIO_PinSource12, GPIO_AF_TIM5);
    GPIO_PinAFConfig(GPIOI, GPIO_PinSource0, GPIO_AF_TIM5);

    TIM_TimeBaseStructure.TIM_Period = PWM_Period;
    TIM_TimeBaseStructure.TIM_Prescaler = PWM_Prescaler;
    TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;

    if (PWM_CounterMode == PWM_CounterMode_Up)
    {
        TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    }

    if (PWM_CounterMode == PWM_CounterMode_Down)
    {
        TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Down;
    }

    TIM_TimeBaseInit(TIM5, &TIM_TimeBaseStructure);

    if (PWM_OCMode == PWMO_OCMode_PWM1)
    {
        TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
    }

    if (PWM_OCMode == PWM_OCMode_PWM2)
    {
        TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;
    }

    if (PWM_OCPolarity == PWM_OCPolarity_High)
    {
        TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
    }

    if (PWM_OCPolarity == PWM_OCPolarity_Low)
    {
        TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
    }

    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = PWM_Pluse;

    TIM_OC1Init(TIM5, &TIM_OCInitStructure);
    TIM_OC1PreloadConfig(TIM5, TIM_OCPreload_Enable);

    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = PWM_Pluse;

    TIM_OC2Init(TIM5, &TIM_OCInitStructure);
    TIM_OC2PreloadConfig(TIM5, TIM_OCPreload_Enable);

    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = PWM_Pluse;

    TIM_OC3Init(TIM5, &TIM_OCInitStructure);
    TIM_OC3PreloadConfig(TIM5, TIM_OCPreload_Enable);

    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = PWM_Pluse;

    TIM_OC4Init(TIM5, &TIM_OCInitStructure);
    TIM_OC4PreloadConfig(TIM5, TIM_OCPreload_Enable);

    TIM_ARRPreloadConfig(TIM5, ENABLE);

    TIM_Cmd(TIM5, ENABLE);
}

/*******************************************************************************
 * @funtion      : Pwm_Motion
 * @description  : 运动控制
 * @param         {int channel} 通道 可选值1-16
 * @param         {double rate} 分辨率 可选值：50、100、150...
 * @param         {int width} 脉宽 可选值：500~2500
 * @return        {*}
 *******************************************************************************/
void Pwm_Motion(int channel, double rate, int width)
{
    uint16_t psc = rate * 100;
    uint16_t pwm = (width * rate) / 100;
    uint16_t TIM_Prescaler;

    switch (channel)
    {
    case 1:
    {
        TIM_Prescaler = ((APB1_TIMER_CLOCKS / PWM_FREQUENCE) / psc - 1);
        TIM4->PSC = TIM_Prescaler;
        TIM_SetCompare1(TIM4, pwm);
        break;
    }
    case 2:
    {
        TIM_Prescaler = ((APB1_TIMER_CLOCKS / PWM_FREQUENCE) / psc - 1);
        TIM4->PSC = TIM_Prescaler;
        TIM_SetCompare2(TIM4, pwm);
        break;
    }
    case 3:
    {
        TIM_Prescaler = ((APB1_TIMER_CLOCKS / PWM_FREQUENCE) / psc - 1);
        TIM4->PSC = TIM_Prescaler;
        TIM_SetCompare3(TIM4, pwm);
        break;
    }
    case 4:
    {
        TIM_Prescaler = ((APB1_TIMER_CLOCKS / PWM_FREQUENCE) / psc - 1);
        TIM4->PSC = TIM_Prescaler;
        TIM_SetCompare4(TIM4, pwm);
        break;
    }
    case 5:
    {
        TIM_Prescaler = ((APB1_TIMER_CLOCKS / PWM_FREQUENCE) / psc - 1);
        TIM5->PSC = TIM_Prescaler;
        TIM_SetCompare1(TIM5, pwm);
        break;
    }
    case 6:
    {
        TIM_Prescaler = ((APB1_TIMER_CLOCKS / PWM_FREQUENCE) / psc - 1);
        TIM5->PSC = TIM_Prescaler;
        TIM_SetCompare2(TIM5, pwm);
        break;
    }
    case 7:
    {
        TIM_Prescaler = ((APB1_TIMER_CLOCKS / PWM_FREQUENCE) / psc - 1);
        TIM5->PSC = TIM_Prescaler;
        TIM_SetCompare3(TIM5, pwm);
        break;
    }
    case 8:
    {
        TIM_Prescaler = ((APB1_TIMER_CLOCKS / PWM_FREQUENCE) / psc - 1);
        TIM5->PSC = TIM_Prescaler;
        TIM_SetCompare4(TIM5, pwm);
        break;
    }
    case 9:
    {
        TIM_Prescaler = ((APB1_TIMER_CLOCKS / PWM_FREQUENCE) / psc - 1);
        TIM2->PSC = TIM_Prescaler;
        TIM_SetCompare1(TIM2, pwm);
        break;
    }
    case 10:
    {
        TIM_Prescaler = ((APB1_TIMER_CLOCKS / PWM_FREQUENCE) / psc - 1);
        TIM2->PSC = TIM_Prescaler;
        TIM_SetCompare2(TIM2, pwm);
        break;
    }
    case 11:
    {
        TIM_Prescaler = ((APB1_TIMER_CLOCKS / PWM_FREQUENCE) / psc - 1);
        TIM2->PSC = TIM_Prescaler;
        TIM_SetCompare3(TIM2, pwm);
        break;
    }
    case 12:
    {
        TIM_Prescaler = ((APB1_TIMER_CLOCKS / PWM_FREQUENCE) / psc - 1);
        TIM2->PSC = TIM_Prescaler;
        TIM_SetCompare4(TIM2, pwm);
        break;
    }
    case 13:
    {
        TIM_Prescaler = ((APB2_TIMER_CLOCKS / PWM_FREQUENCE) / psc - 1);
        TIM8->PSC = TIM_Prescaler;
        TIM_SetCompare1(TIM8, pwm);
        break;
    }
    case 14:
    {
        TIM_Prescaler = ((APB2_TIMER_CLOCKS / PWM_FREQUENCE) / psc - 1);
        TIM8->PSC = TIM_Prescaler;
        TIM_SetCompare2(TIM8, pwm);
        break;
    }
    case 15:
    {
        TIM_Prescaler = ((APB2_TIMER_CLOCKS / PWM_FREQUENCE) / psc - 1);
        TIM8->PSC = TIM_Prescaler;
        TIM_SetCompare3(TIM8, pwm);
        break;
    }
    case 16:
    {
        TIM_Prescaler = ((APB2_TIMER_CLOCKS / PWM_FREQUENCE) / psc - 1);
        TIM8->PSC = TIM_Prescaler;
        TIM_SetCompare4(TIM8, pwm);
        break;
    }
    }
}

/*******************************************************************************
 * @funtion      : Pwm_Usb_Callback
 * @description  : 串口任务回调函数
 * @param         {int channel} 通道 可选值1-16
 * @param         {double rate} 分辨率 可选值：50、100、150...
 * @param         {int width} 脉宽 可选值：500~2500
 * @return        {*}
 *******************************************************************************/
void Pwm_Usb_Callback(char *type, int channel, double rate, int width)
{
    if (memcmp(type, "pwm-control", 11) == 0)
    {
        Pwm_Motion(channel, rate, width);
    }
}
