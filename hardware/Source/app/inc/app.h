/**
 ******************************************************************************
 * @file    app.h
 * @author  GEEKROS,  site:www.geekros.com
 ******************************************************************************
 */

#ifndef APP
#define APP

#include "utils.h"
#include "modular.h"
#include "robot.h"

void Setup(void);

void Loop(void);

#endif
