#ifndef ROBOT_ARM_AIR
#define ROBOT_ARM_AIR

#include "utils.h"
#include "modular.h"

typedef struct
{
    int Status; // 某个电机校准状态
    int Position; // 某个电机目标位置
    int Speed; // 某个电机目标转速
    float   Offset;
}Arm_Motor_Struct;

extern Arm_Motor_Struct Motor_Data[CAN_MOTOR_LEN];

void Arm_Init(void);

int16_t Arm_Format_Send_Data(int Data);

void Arm_Motor_Start(void);

void Arm_Motor_Stop(void);

void Arm_Motor_Motion_Format(void);

void Arm_Motor_Motion(int StdId, int16_t Data1, int16_t Data2, int16_t Data3, int16_t Data4);

void Motor_Start_Up(void);

void Motor_Calibration_Motion(int action);

void Arm_Task(void);

void Arm_Usb_Callback(char *type);

#endif
