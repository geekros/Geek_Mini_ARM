/**
 ******************************************************************************
 * @file    robot.c
 * @author  GEEKROS,  site:www.geekros.com
 ******************************************************************************
 */

#include "robot.h"

Robot_Struct Robot;

/*******************************************************************************
 * @funtion      : Robot_Type
 * @description  : 机器人类型设置函数
 * @param         {int robot_type} 机器人类型
 * @return        {*}
 *******************************************************************************/
void Robot_Type(int robot_type)
{
    Arm_Init();
}

/*******************************************************************************
 * @funtion      : Robot_Task
 * @description  : 机器人任务函数
 * @param         {*} 机器人类型
 * @return        {*}
 *******************************************************************************/
void Robot_Task(void)
{
	Arm_Task();
}

/*******************************************************************************
 * @funtion      : Robot_Usb_Callback
 * @description  : 串口任务回调函数
 * @param         {char *type, int robot_typ} 通讯协议类型,机器人类型
 * @return        {*}
 *******************************************************************************/
void Robot_Usb_Callback(char *type, int robot_type)
{
    if (memcmp(type, "robot-type", 10) == 0)
    {
        Robot_Type(robot_type);
    }
}
