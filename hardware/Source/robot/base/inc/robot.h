/**
 ******************************************************************************
 * @file    robot.h
 * @author  GEEKROS,  site:www.geekros.com
 ******************************************************************************
 */

#ifndef ROBOT
#define ROBOT

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "utils.h"
#include "arm_air.h"

typedef enum
{
	ROBOT_NONE = 0,
} Robot_Type_Struct;

typedef struct
{
	Robot_Type_Struct Robot_Type;
} Robot_Struct;

extern Robot_Struct Robot;

void Robot_Type(int robot_type);

void Robot_Task(void);

void Robot_Usb_Callback(char *type, int robot_type);

#endif
