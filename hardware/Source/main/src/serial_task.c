/**
 ******************************************************************************
 * @file    serial_task.c
 * @author  GEEKROS,  site:www.geekros.com
 ******************************************************************************
 */

#include "serial_task.h"

Serial_Data_Struct Serial_Read_Data;

uint32_t Serial_Task_Buffer_Len = 1024;

uint8_t Serial_Task_Buffer[1024];

/*******************************************************************************
 * @funtion      : Serial_Task
 * @description  : USB虚拟串口任务函数
 * @param         {*}
 * @return        {*}
 *******************************************************************************/
void Serial_Task(void)
{
	while (1)
	{

		// 拿到USB虚拟串口的实时数据
		Usb_Read_Data((uint8_t *)Serial_Task_Buffer, Serial_Task_Buffer_Len);

		// 进行JSON通讯协议解析与处理
		Serial_Json_Handle((char *)Serial_Task_Buffer);

		delay_ms(20);
	}
}

/*******************************************************************************
 * @funtion      : Serial_Json_Handle
 * @description  : JSON通讯协议解析与处理函数
 * @param         {char *json} JSON通讯协议
 * @return        {*}
 *******************************************************************************/
void Serial_Json_Handle(char *json)
{
	// 解析USB虚拟串口数据
	cJSON *Serial_Data;
	Serial_Data = cJSON_Parse(json);
	if (Serial_Data)
	{

		// 通讯协议类型
		Serial_Read_Data.type = cJSON_GetObjectItem(Serial_Data, "type")->valuestring;

		// LED模块
		if (memcmp(Serial_Read_Data.type, "led", 3) == 0)
		{
			Serial_Read_Data.led.channel = cJSON_GetObjectItem(Serial_Data, "channel")->valuestring;
			Serial_Read_Data.led.state = cJSON_GetObjectItem(Serial_Data, "state")->valuestring;
			Led_Usb_Callback(Serial_Read_Data.type, Serial_Read_Data.led.channel, Serial_Read_Data.led.state);
		}

		// CAN模块
		if (memcmp(Serial_Read_Data.type, "can", 3) == 0)
		{
			Serial_Read_Data.can.channel = cJSON_GetObjectItem(Serial_Data, "channel")->valueint;
			Serial_Read_Data.can.motor_id = cJSON_GetObjectItem(Serial_Data, "motor_id")->valueint;
			Can_Usb_Callback(Serial_Read_Data.type, Serial_Read_Data.can.channel, Serial_Read_Data.can.motor_id);
		}

		// POWER模块
		if (memcmp(Serial_Read_Data.type, "power", 5) == 0)
		{
			Serial_Read_Data.power.channel = cJSON_GetObjectItem(Serial_Data, "channel")->valueint;
			Serial_Read_Data.power.id = cJSON_GetObjectItem(Serial_Data, "id")->valueint;
			Serial_Read_Data.power.state = cJSON_GetObjectItem(Serial_Data, "state")->valuestring;
			Power_Usb_Callback(Serial_Read_Data.type, Serial_Read_Data.power.channel, Serial_Read_Data.power.id, Serial_Read_Data.power.state);
		}

		// KEY模块
		if (memcmp(Serial_Read_Data.type, "key", 3) == 0)
		{
			Key_Usb_Callback(Serial_Read_Data.type);
		}

		// ADC模块
		if (memcmp(Serial_Read_Data.type, "adc", 3) == 0)
		{
			Serial_Read_Data.adc.voltage = cJSON_GetObjectItem(Serial_Data, "voltage")->valuedouble;
			Adc_Usb_Callback(Serial_Read_Data.type, Serial_Read_Data.adc.voltage);
		}

		// IO模块
		if (memcmp(Serial_Read_Data.type, "io", 2) == 0)
		{
			Serial_Read_Data.io.channel = cJSON_GetObjectItem(Serial_Data, "channel")->valueint;
			Serial_Read_Data.io.mode = cJSON_GetObjectItem(Serial_Data, "mode")->valuestring;
			Serial_Read_Data.io.state = cJSON_GetObjectItem(Serial_Data, "state")->valuestring;
			Io_Usb_Callback(Serial_Read_Data.type, Serial_Read_Data.io.channel, Serial_Read_Data.io.mode, Serial_Read_Data.io.state);
		}

		// PWM模块
		if (memcmp(Serial_Read_Data.type, "pwm", 3) == 0)
		{
			Serial_Read_Data.pwm.channel = cJSON_GetObjectItem(Serial_Data, "channel")->valueint;
			Serial_Read_Data.pwm.rate = cJSON_GetObjectItem(Serial_Data, "rate")->valuedouble;
			Serial_Read_Data.pwm.width = cJSON_GetObjectItem(Serial_Data, "width")->valueint;
			Pwm_Usb_Callback(Serial_Read_Data.type, Serial_Read_Data.pwm.channel, Serial_Read_Data.pwm.rate, Serial_Read_Data.pwm.width);
		}

		// CPU模块
		if (memcmp(Serial_Read_Data.type, "cpu", 3) == 0)
		{
			Cpu_Usb_Callback(Serial_Read_Data.type);
		}

		// MPU模块
		if (memcmp(Serial_Read_Data.type, "mpu", 3) == 0)
		{
			Mpu_Usb_Callback(Serial_Read_Data.type);
		}
		
		// FLASH模块
		if (memcmp(Serial_Read_Data.type, "flash", 5) == 0)
		{
			Serial_Read_Data.flash.len = cJSON_GetObjectItem(Serial_Data, "len")->valueint;
			Flash_Usb_Callback(Serial_Read_Data.type, Serial_Read_Data.flash.len);
		}

		// HMI模块
		if (memcmp(Serial_Read_Data.type, "hmi", 3) == 0)
		{
			Serial_Read_Data.hmi.rate = cJSON_GetObjectItem(Serial_Data, "rate")->valueint;
			Hmi_Usb_Callback(Serial_Read_Data.type, Serial_Read_Data.hmi.rate);
		}

		// WIFI模块
		if (memcmp(Serial_Read_Data.type, "wifi", 4) == 0)
		{
			Serial_Read_Data.wifi.rate = cJSON_GetObjectItem(Serial_Data, "rate")->valueint;
			Wifi_Usb_Callback(Serial_Read_Data.type, Serial_Read_Data.wifi.rate);
		}

		// SERVO模块
		if (memcmp(Serial_Read_Data.type, "servo", 5) == 0)
		{
			Serial_Read_Data.servo.id = cJSON_GetObjectItem(Serial_Data, "id")->valueint;
			Serial_Read_Data.servo.baud_rate = cJSON_GetObjectItem(Serial_Data, "rate")->valueint;
			Serial_Read_Data.servo.servo_type = cJSON_GetObjectItem(Serial_Data, "servo_type")->valueint;
			Servo_Usb_Callback(Serial_Read_Data.type, Serial_Read_Data.servo.id, Serial_Read_Data.servo.baud_rate, Serial_Read_Data.servo.servo_type);
		}

		// 遥控器模块
		if (memcmp(Serial_Read_Data.type, "remote-control", 14) == 0)
		{
			Serial_Read_Data.rocker_control.rate = cJSON_GetObjectItem(Serial_Data, "rate")->valueint;
			Serial_Read_Data.rocker_control.s1 = cJSON_GetObjectItem(Serial_Data, "s1")->valueint;
			Serial_Read_Data.rocker_control.s2 = cJSON_GetObjectItem(Serial_Data, "s2")->valueint;
			Serial_Read_Data.rocker_control.ch0 = cJSON_GetObjectItem(Serial_Data, "ch0")->valuedouble;
			Serial_Read_Data.rocker_control.ch1 = cJSON_GetObjectItem(Serial_Data, "ch1")->valuedouble;
			Serial_Read_Data.rocker_control.ch2 = cJSON_GetObjectItem(Serial_Data, "ch2")->valuedouble;
			Serial_Read_Data.rocker_control.ch3 = cJSON_GetObjectItem(Serial_Data, "ch3")->valuedouble;
			Rocker_Write_Data(Serial_Read_Data.rocker_control.s1, Serial_Read_Data.rocker_control.s2, Serial_Read_Data.rocker_control.ch0, Serial_Read_Data.rocker_control.ch1, Serial_Read_Data.rocker_control.ch2, Serial_Read_Data.rocker_control.ch3);
		}
		
		// Pwm电机模块
		if (memcmp(Serial_Read_Data.type, "motor", 5) == 0)
		{
			int Motor_Num = cJSON_GetObjectItem(Serial_Data, "Num")->valueint;
			int Motor_Fre = cJSON_GetObjectItem(Serial_Data, "Fre")->valueint;
			int Motor_Spe = cJSON_GetObjectItem(Serial_Data, "Speed")->valueint;
			Pwm_Motor_Speed(Motor_Num, Motor_Fre, Motor_Spe);
		}

		// ROBOT模块
		if (memcmp(Serial_Read_Data.type, "robot", 5) == 0)
		{
			Serial_Read_Data.robot.robot_type = cJSON_GetObjectItem(Serial_Data, "robot_type")->valueint;
			Robot_Usb_Callback(Serial_Read_Data.type, Serial_Read_Data.robot.robot_type);
		}
        
        // ARM模块
        if (memcmp(Serial_Read_Data.type, "arm", 3) == 0)
        {
            
        }
	}

	// 需要清空本次收到的数据
	cJSON_Delete(Serial_Data);
	for (int i = 0; i < Serial_Task_Buffer_Len; i++)
	{
		if (Serial_Task_Buffer[i] == NULL)
		{
			break;
		}else{
			Serial_Task_Buffer[i] = NULL;
		}
	}
}
