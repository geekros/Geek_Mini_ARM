/**
 ******************************************************************************
 * @file    main.c
 * @author  GEEKROS,  site:www.geekros.com
 ******************************************************************************
 */

#include "main.h"

/*******************************************************************************
 * @funtion      : main
 * @description  : 程序启动入口
 * @param         {*}
 * @return        {*}
 *******************************************************************************/
int main(void)
{
    // 时钟初始化
    Delay_Init(configTICK_RATE_HZ);

    // USB虚拟串口初始化
    Usb_Init();

    // CPU模块初始化
    Cpu_Init();

    // LED模块初始化
    Led_Init();

    // CAN模块初始化
    Can_Init();

    // POWER模块初始化
    Power_Init();

    // KEY模块初始化
    Key_Init();

    // PWM模块初始化
    Pwm_Init();

    // ADC模块初始化
    Adc_Init();

    // MPU模块初始化
    Mpu_Init();

    // BUZZER模块初始化
    Buzzer_Init();

    // 任务管理器
    Task_Manage();

    vTaskStartScheduler();
}

/*******************************************************************************
 * @funtion      : Start_Task
 * @description  : 任务管理器
 * @param         {*}
 * @return        {*}
 *******************************************************************************/
#define START_TASK_PRIO 1
#define START_TASK_SIZE 1024
static TaskHandle_t Start_Task_Handler;
void Task_Manage(void)
{
    // 启动任务
    xTaskCreate((TaskFunction_t)Start_Task, (const char *)"Start_Task", (uint16_t)START_TASK_SIZE, (void *)NULL, (UBaseType_t)START_TASK_PRIO, (TaskHandle_t *)&Start_Task_Handler);
}

/*******************************************************************************
 * @funtion      : Task_Manage
 * @description  : 启动任务
 * @param         {*}
 * @return        {*}
 *******************************************************************************/
#define SYSTEM_TASK_PRIO 2
#define SYSTEM_TASK_SIZE 256
static TaskHandle_t System_Task_Handler;
#define VOLTAGE_TASK_PRIO 2
#define VOLTAGE_TASK_SIZE 256
static TaskHandle_t Voltage_Task_Handler;
#define SERIAL_TASK_PRIO 3
#define SERIAL_TASK_SIZE 1024
static TaskHandle_t Serial_Task_Handler;
#define USER_TASK_PRIO 4
#define USER_TASK_SIZE 1024
static TaskHandle_t User_Task_Handler;
void Start_Task(void *pvParameters)
{
    taskENTER_CRITICAL();

    // 创建系统任务
    xTaskCreate((TaskFunction_t)System_Task, (const char *)"System_Task", (uint16_t)SYSTEM_TASK_SIZE, (void *)NULL, (UBaseType_t)SYSTEM_TASK_PRIO, (TaskHandle_t *)&System_Task_Handler);
		
	// 电量检测任务
	xTaskCreate((TaskFunction_t)Voltage_Task, (const char *)"Voltage_Task", (uint16_t)VOLTAGE_TASK_SIZE, (void *)NULL, (UBaseType_t)VOLTAGE_TASK_PRIO, (TaskHandle_t *)&Voltage_Task_Handler);

    // 创建串口任务
    xTaskCreate((TaskFunction_t)Serial_Task, (const char *)"Serial_Task", (uint16_t)SERIAL_TASK_SIZE, (void *)NULL, (UBaseType_t)SERIAL_TASK_PRIO, (TaskHandle_t *)&Serial_Task_Handler);

    // 创建用户任务
    xTaskCreate((TaskFunction_t)User_Task, (const char *)"User_Task", (uint16_t)USER_TASK_SIZE, (void *)NULL, (UBaseType_t)USER_TASK_PRIO, (TaskHandle_t *)&User_Task_Handler);

    vTaskDelete(Start_Task_Handler);

    taskEXIT_CRITICAL();
}
