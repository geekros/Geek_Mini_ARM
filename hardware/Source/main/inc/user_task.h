/**
 ******************************************************************************
 * @file    user_task.h
 * @author  GEEKROS,  site:www.geekros.com
 ******************************************************************************
 */

#ifndef USER_TASK
#define USER_TASK

#include "utils.h"
#include "modular.h"
#include "app.h"

void Heart_Task(void);

void User_Task(void);

#endif
