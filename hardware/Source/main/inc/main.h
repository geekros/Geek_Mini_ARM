/**
  ******************************************************************************
  * @file    main.h
  * @author  GEEKROS,  site:www.geekros.com
  ******************************************************************************
*/

#ifndef MAIN
#define MAIN

#include "utils.h"
#include "modular.h"
#include "robot.h"
#include "system_task.h"
#include "serial_task.h"
#include "user_task.h"

void Task_Manage(void);

void Start_Task(void *pvParameters);

#endif
