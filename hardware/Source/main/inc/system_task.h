/**
  ******************************************************************************
  * @file    system_task.h
  * @author  GEEKROS,  site:www.geekros.com
  ******************************************************************************
*/

#ifndef SYSTEM_TASK
#define SYSTEM_TASK

#include "utils.h"
#include "modular.h"

void System_Task(void);

void Voltage_Task(void);

#endif
