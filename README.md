# Geek_Mini_ARM

> 该仓库开源资料、程序已转移至GeekStudio，请在GeekStudio的商城中获取详细信息。

## GeekStudio使用说明

<p align="center">
  <img alt="RapidAPI" src="https://cdn.geekros.com/geek/resources/git/studio-index.jpg">
</p>

> GeekStudio是GeekROS团队研发的机器人开发工具，GeekROS是一款深度融合国产技术生态的机器人低代码开发平台，致力于让机器人的开发学习变得更简单。

> 你在可在VSCode的插件扩展中心搜索“GeekStudio”进行安装，首次使用请仔细阅读GeekStudio的入门文档：https://marketplace.visualstudio.com/items?itemName=GEEKROS.geekstudio