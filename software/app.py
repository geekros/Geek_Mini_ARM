#!/usr/bin/env python
# -*- coding: utf-8 -*-

import framework as frame

def sdk_start(sdk):
    sdk.vision.set_model("gesture_detection")
    while True:
        sdk.common.time.sleep(1)
        sdk.log("start")

if __name__ == '__main__':
    sdk = frame.Init()
    sdk_start(sdk)