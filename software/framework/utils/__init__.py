from .device.device import Device
from .message.message import Message

class Utils:

    def __init__(self, framework):
        self.message = Message(framework)
        self.device = Device(framework)