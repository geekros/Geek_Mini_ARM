class Robot:

    def __init__(self, framework):
        self._framework = framework
        self.command = {}

    def start(self, robot_type):
        self.command = {"type": "robot-type", "robot_type": robot_type}
        if self._framework.utils.device.device_connect:
            self._framework.utils.device.device_write(self.command)

    def message_callback(self, message):
        pass