class Adc:

    def __init__(self, framework):
        self._framework = framework
        self.command = {}
        self.voltage = 0.00
        self.pwm_voltage = 0.00
        self.cpu_temperature = 0.00

    def set_low_voltage(self, voltage):
        self.command = {"type": "adc-low-voltage", "voltage": voltage}
        if self._framework.utils.device.device_connect:
            self._framework.utils.device.device_write(self.command)

    def get_board_voltage(self):
        self.command = {"type": "adc-board-voltage"}
        if self._framework.utils.device.device_connect:
            self._framework.utils.device.device_write(self.command)

    def get_pwm_voltage(self):
        self.command = {"type": "adc-pwm-voltage"}
        if self._framework.utils.device.device_connect:
            self._framework.utils.device.device_write(self.command)
