class Servo:

    def __init__(self, framework):
        self._framework = framework
        self.command = {}

    def set_rate(self, servo_id, rate, servo_type):
        self.command = {"type": "servo-rate", "id": servo_id, "rate": rate, "servo_type": servo_type}
        if self._framework.utils.device.device_connect:
            self._framework.utils.device.device_write(self.command)
