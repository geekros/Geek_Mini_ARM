class Key:

    def __init__(self, framework):
        self._framework = framework
        self.command = {}
        self.key_state = 0

    def get_state(self):
        self.command = {"type": "key-state"}
        if self._framework.utils.device.device_connect:
            self._framework.utils.device.device_write(self.command)
