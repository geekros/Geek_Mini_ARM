class Led:

    def __init__(self, framework):
        self._framework = framework
        self.command = {}

    def set_state(self, channel, state):
        self.command = {"type": "led-state", "channel": channel, "state": state}
        if self._framework.utils.device.device_connect:
            self._framework.utils.device.device_write(self.command)
