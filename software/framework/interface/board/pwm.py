class Pwm:

    def __init__(self, framework):
        self._framework = framework
        self.command = {}

    def pwm_control(self, channel, rate, width):
        self.command = {"type": "pwm-control", "channel": channel, "rate": rate, "width": width}
        if self._framework.utils.device.device_connect:
            self._framework.utils.device.device_write(self.command)
