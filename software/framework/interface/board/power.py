class Power:

    def __init__(self, framework):
        self._framework = framework
        self.command = {}

    def set_state(self, channel, channel_id, state):
        self.command = {"type": "power-state", "channel": channel, "id": channel_id, "state": state}
        if self._framework.utils.device.device_connect:
            self._framework.utils.device.device_write(self.command)
