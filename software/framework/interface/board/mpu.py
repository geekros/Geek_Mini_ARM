class Mpu:

    def __init__(self, framework):
        self._framework = framework
        self.command = {}
        self.temperature = 0.00
        self.altitude = 0.00
        self.pressure = 0.00
        self.rol = 0.00
        self.pit = 0.00
        self.yaw = 0.00

    def get_data(self, data):
        return_data = 0.00
        if data == "rol":
            return_data = self.rol
        if data == "pit":
            return_data = self.pit
        if data == "yaw":
            return_data = self.yaw
        if data == "temperature":
            return_data = self.temperature
        if data == "altitude":
            return_data = self.altitude
        if data == "pressure":
            return_data = self.pressure
        return return_data

    def read_data(self):
        self.command = {"type": "mpu-read"}
        if self._framework.utils.device.device_connect:
            self._framework.utils.device.device_write(self.command)
