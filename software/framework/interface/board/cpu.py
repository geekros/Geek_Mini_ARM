class Cpu:

    def __init__(self, framework):
        self._framework = framework
        self.command = {}
        self.temperature = 0.00

    def get_temperature(self):
        self.command = {"type": "cpu-temperature"}
        if self._framework.utils.device.device_connect:
            self._framework.utils.device.device_write(self.command)
