class Rocker:

    def __init__(self, framework):
        self._framework = framework
        self.command = {}
        self.ch0 = 0
        self.ch1 = 0
        self.ch2 = 0
        self.ch3 = 0
        self.ch4 = 0
        self.s1 = 3
        self.s2 = 3

    def set_rate(self, rate):
        self.command = {"type": "rocker-control-rate", "rate": rate}
        if self._framework.utils.device.device_connect:
            self._framework.utils.device.device_write(self.command)

    def read_data(self):
        self.command = {"type": "rocker-control-data"}
        if self._framework.utils.device.device_connect:
            self._framework.utils.device.device_write(self.command)

