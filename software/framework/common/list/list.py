import json

class List:

    def __init__(self, framework):
        self._framework = framework
        self.list = []

    def init(self):
        self.list = []

    def set(self, list_data):
        self.list = json.loads(list_data)
        return self.list
