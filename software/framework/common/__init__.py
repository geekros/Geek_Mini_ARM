import platform
from .time.time import Time
from .random.random import Random
from .list.list import List

class Common:

    def __init__(self, framework):
        self.time = Time(framework)
        self.random = Random(framework)
        self.list = List(framework)
        self.platform = platform.system().lower()
