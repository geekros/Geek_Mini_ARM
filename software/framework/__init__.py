import sys
from .common import Common
from .utils import Utils
from .vision import Vision
from .interface.board import Board
from .interface.robot import Robot

class Init:

    def __init__(self):
        self.common = Common(self)
        self.utils = Utils(self)
        self.board = Board(self)
        self.robot = Robot(self)
        self.vision = Vision(self)

    # 通过串口方式连接设备
    def connect_serial_device(self, address, port):
        self.utils.device.connect_device("serial", address, port)

    # 通过网络方式连接设备
    def connect_net_device(self, address, port):
        self.utils.device.connect_device("net", address, port)

    # 输出调试日志
    def log(self, data):
        self.utils.message.log(data)
