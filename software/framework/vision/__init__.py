import platform
from .detection.fcos import Fcos
from .detection.body import Body
from .detection.hand_lmk import HandLmk
from .detection.gesture import Gesture
from .classification.mobilenetv2 import Mobilenetv2
from .fragmentation.mobilenet_unet import MobilenetUnet
from .three.mono3d import Mono3D
from .elevation.net_parser import NetParser
import app as robot_script

class Vision:

    def __init__(self, framework):
        self._framework = framework
        self.platform = platform.system()
        self.detection_fcos = Fcos()
        self.detection_body = Body()
        self.detection_hand_lmk = HandLmk()
        self.detection_gesture = Gesture()
        self.classification_mobilenetv2 = Mobilenetv2()
        self.fragmentation_mobilenet_unet = MobilenetUnet()
        self.three_mono3d = Mono3D()
        self.elevation_net_parser = NetParser()
        self.func = dir(robot_script)

    def set_model(self, model):
        send_json = {"operate": "set_vision_model", "node": []}
        if model and model != "":
            if model == "fcos":
                send_json["node"] = self.detection_fcos.get_node()
            if model == "mobilenetv2":
                send_json["node"] = self.classification_mobilenetv2.get_node()
            if model == "mobilenet_unet":
                send_json["node"] = self.fragmentation_mobilenet_unet.get_node()
            if model == "body_detection":
                send_json["node"] = self.detection_body.get_node()
            if model == "lmk_detection":
                send_json["node"] = self.detection_hand_lmk.get_node()
            if model == "gesture_detection":
                send_json["node"] = self.detection_gesture.get_node()
            if model == "mono3d_detection":
                send_json["node"] = self.three_mono3d.get_node()
            if model == "elevation_detection":
                send_json["node"] = self.elevation_net_parser.get_node()
        self._framework.common.time.sleep(0.5)
        self._framework.utils.message.write(send_json)

    def vision_callback(self, message):
        pass
