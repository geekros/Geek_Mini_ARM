class Gesture:

    def __init__(self):
        self.node_json = []

    def get_node(self):
        self.node_json = [
            {
                "package": "mono2d_body_detection",
                "executable": "mono2d_body_detection",
                "output": "",
                "parameters": [],
                "arguments": ["--ros-args", "--log-level", "error"]
            },
            {
                "package": "hand_lmk_detection",
                "executable": "hand_lmk_detection",
                "output": "",
                "parameters": [],
                "arguments": ["--ros-args", "--log-level", "error"]
            },
            {
                "package": "hand_gesture_detection",
                "executable": "hand_gesture_detection",
                "output": "",
                "parameters": [],
                "arguments": ["--ros-args", "--log-level", "error"],
                "smart_topic": "/hobot_hand_gesture_detection"
            }
        ]
        return self.node_json
